import React from 'react'
import ReactDOM from 'react-dom';
import certificate from '../image/certificate.png'
import qr from '../image/qr.png'
import mysign from '../image/mysign.png'
import jsPDF from 'jspdf'
import logoo from '../image/logoo.png'
import html2canvas from 'html2canvas';

export default function Course() {


 

const generatePDFs=()=> {

  alert ( "hello world ")
    // var doc = new jsPDF('p', 'pt', 'letter');
//       var doc = new jsPDF("p", "mm", "l");

// var width = doc.internal.pageSize.getWidth();
// var height = doc.internal.pageSize.getHeight();

//     doc.html(document.body, {
//       callback: function (doc) {
//         doc.save('ReanCodeCertificate.pdf');
//       }
//     })
 

  }
 

  const exportPDF = () => {

    alert(" Export the pdf images -------------------------")
    const input = document.getElementById("App")
    html2canvas(input, { logging: true, letterRendering: 1, useCORS: true }).then(
      canvas => {
        // const imgWidth = 208; 
        const imgWidth = 296.5; 
        const imgHeight =((canvas.height) * imgWidth / canvas.width) ; 
        const imgData = canvas.toDataURL('image/png')
        
        const pdf = new jsPDF('l', 'mm', 'a4')
        pdf.addImage(imgData, 'PNG', 0, 0, imgWidth, imgHeight-5.5)
        pdf.save("nothing.pdf")
        
      }
    )
     
   }
 
  return (

    <header id='App' className='App-header ' >
 <div id="course" className="budgeting-pdf"  >
        <div className="flex justify-center items-center bg-red rounded-full sm:w md:w lg:w xl:w mb-4">
            {/* <div className="card-body bg-gradient-to-r from-cyan-500 via-orange to-amber-500 w-full rounded"> */}
            <div className="card-body border-4 ring-offset-2 ring-4 border-orange bg-blue-400 rounded">
                <div className="grid grid-cols-3 gap-4">
                  <div className="one">
                    <img src={certificate} className="w-170 h-80 rounded-full" />
                  </div>

                  <div className="grid grid-cols-1 gap-4">
                    <div className="col-span-2">
                      <p className="card-title text-lg pt-10"><b>Certificate
                      of <br />
                      Course Name</b></p>
                      <br />
                      <p className="text-base">This certifies that</p>
                      <br />
                      <p className="card-title text-xl"><b>Student Name
                       <br /> Sex <br /> Score
                      </b></p>

                      <br />
                      <p className="text-base">has completed the necessary course of study and passed on ReanCode</p>
                      <p className="text-base"> Course Name Exercise and is hereby declared a</p>
                      <br />
                      <p className="card-title text-xl"><b>Certified Course Name Developer</b></p>
                      <br />
                      <p className="text-base">with fundamental knowledge of web development using advanced course name</p>
                    </div>
                  </div>
                  
                  <div className="grid grid-cols-6 gap-4">
                    <div className="col-end-6 col-span-3 pt-4">
                      <img src={qr} className="w-120 h-50" />
                    </div>
                  </div>
                </div>

                <br />
                <div className="grid grid-cols-6 gap-4">
                  <div className="col-start-1 col-end-3 pt-20">
                    <input type="current-date" />
                    <p>verify completion at</p>
                    <p>https address</p> <br />
                    <button 
                    onClick={()=> exportPDF()}
                    className="bg-orange hover:bg-pink-200 text-white font-bold py-2 px-4 rounded-full">
                      Download
                    </button>
                  </div>

                  <div className="col-end-7 col-span-2">
                    <img src={mysign} className="md:w" />
                    <hr />
                    <p className="text-sm">Sao Horngly <br/> Admin of ReanCode</p>
                    <p className='pb-2'>for www.reancodes.com</p>
                  </div>
                </div>
            </div>
        </div>
    </div>
    </header>
   
  )
}
